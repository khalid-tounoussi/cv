const newspaperSpinning = [
    { transform: 'scale(0.5)' },
    { transform: 'scale(1)' }
];
var values;

const newspaperTiming = {
    duration: 2000,
    iterations: 1,
}

function toogleDetails() {
    var acc = document.getElementsByClassName("accordion");
    var i;
    for (i = 0; i < acc.length; i++) {

        acc[i].addEventListener("click", function () {
            for (let element of acc) {
                if (element != acc[i]) {
                    element.classList.remove("active");
                    var panel = element.nextElementSibling;
                    panel.style.display = "none";
                }

            };
            this.classList.toggle("active");

            this.nextElementSibling.getElementsByTagName("p")[0].animate(newspaperSpinning, newspaperTiming);

            var panel = this.nextElementSibling;
            if (panel.style.display === "block") {
                panel.style.display = "none";
            } else {
                panel.style.display = "block";
            }

            this.style.transfotm

        });
    }

}

function showDetailsOnHover() {
    let acc = document.querySelector("article div dl");
    let elements = acc.querySelectorAll("dt,dd")
    for (let element of elements) {
        if (element.tagName == "DT") {
            element.addEventListener('mouseover', () => {
                element.nextElementSibling.classList.add("show");
            }, false)

            element.addEventListener('mouseleave', () => {
                element.nextElementSibling.classList.remove("show");
            }, false)



            document.addEventListener('mousemove', function (e) {

                element.nextElementSibling.style.left =
                    (e.pageX + element.nextElementSibling.clientWidth + 10 < document.body.clientWidth)
                        ? (e.pageX + 10 + "px")
                        : (document.body.clientWidth + 5 - element.nextElementSibling.clientWidth + "px");
                element.nextElementSibling.style.top =
                    (e.pageY + element.nextElementSibling.clientHeight + 10 < document.body.clientHeight)
                        ? (e.pageY + 10 + "px")
                        : (document.body.clientHeight + 5 - element.nextElementSibling.clientHeight + "px");
            });
        }


    }

}

const ratingStars = [...document.getElementsByClassName("rating__star")];

const ratings = [...document.getElementsByClassName("rating")]

function executeRating() {
    const starClassActive = "rating__star fas fa-star";
    const starClassUnactive = "rating__star far fa-star";
    let i;
    ratings.map((rating) => {
        const stars = [...rating.getElementsByClassName("rating__star")]
        var starsLength = stars.length
        stars.map((star) => {
            star.onclick = () => {
                i = stars.indexOf(star);

                if (star.className.indexOf(starClassUnactive) !== -1) {
                    rating.setAttribute("value", i + 1)

                    for (i; i >= 0; --i) stars[i].className = starClassActive;
                } else {
                    rating.setAttribute("value", i)

                    for (i; i < starsLength; ++i) stars[i].className = starClassUnactive;
                }
                drawHistogram()
            }
        });
    });
}
function drawHistogram() {
    const ctx = document.getElementById('histogram').getContext('2d');

    const labels = [...document.querySelectorAll(".comp-name")]
        .map((com) => com.innerText)

    const values = [...document.getElementsByClassName("rating")]
        .map((rating) => rating.getAttribute("value"))



    const chart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: labels,
            datasets: [{
                label: 'Auto évaluation',
                data: values,
                backgroundColor: 'navy',
            }]
        },
        options: {
            scales: {

                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
}

executeRating(ratingStars);
toogleDetails()
showDetailsOnHover()